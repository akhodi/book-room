FactoryBot.define do
  factory :meeting_room do
    name { Faker::Name.unique.name }
  end
end
