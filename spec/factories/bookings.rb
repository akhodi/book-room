FactoryBot.define do
  factory :booking do
    association :user, factory: :user
    association :meeting_room, factory: :meeting_room
    booking_date { Time.now }
  end
end
