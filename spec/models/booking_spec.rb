require 'rails_helper'

RSpec.describe Booking, type: :model do
  it { is_expected.to validate_presence_of(:user_id) }
  it { is_expected.to validate_presence_of(:meeting_room_id) }
  it { is_expected.to validate_presence_of(:booking_date) }
end
