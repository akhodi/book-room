class CreateMeetingRoom < ActiveRecord::Migration[6.0]
  def change
    create_table :meeting_rooms do |t|
      t.timestamps
      
      t.string :name
    end
  end
end
