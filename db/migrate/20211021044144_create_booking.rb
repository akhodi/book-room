class CreateBooking < ActiveRecord::Migration[6.0]
  def change
    create_table :bookings do |t|
      t.timestamps

      t.references :user, null: true, foreign_key: true
      t.references :meeting_room, null: true, foreign_key: true
      t.date :booking_date
    end
  end
end
