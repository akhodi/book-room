unless MeetingRoom.all.present?
  beginning_time = Time.now

  4.times do
    MeetingRoom.create!(
      {
        name: Faker::Name.initials(3)
      }
    )
  end

  end_time = Time.now
  puts "🎉 MeetingRoom [OK] -> " + "#{(((end_time - beginning_time) * 1000) / 1000).to_s[0..3]} seconds"
end
