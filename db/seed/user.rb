unless User.all.present?
  beginning_time = Time.now

  3.times do
    User.create!(
      {
        name: Faker::Name.unique.name,
      }
    )
  end

  end_time = Time.now
  puts "🎉 User [OK] -> " + "#{(((end_time - beginning_time) * 1000) / 1000).to_s[0..3]} seconds"
end
