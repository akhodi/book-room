require 'database_cleaner'

DatabaseCleaner.clean_with(:truncation)

load 'db/seed/user.rb'
load 'db/seed/meeting_room.rb'