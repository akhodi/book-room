class BookingSerializer < ActiveModel::Serializer
  attributes :id, :user_id, :meeting_room_id, :booking_date
end
