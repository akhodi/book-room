class Booking < ApplicationRecord
    belongs_to :user
    belongs_to :meeting_room

    validates :user_id, :meeting_room_id, :booking_date, presence: true
end
