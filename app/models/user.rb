class User < ApplicationRecord
  has_many :bookings, dependent: :destroy, foreign_key: 'user_id'

  validates :name, presence: true
end
