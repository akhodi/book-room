class MeetingRoom < ApplicationRecord
  has_many :bookings, dependent: :destroy, foreign_key: 'meeting_room_id'

  validates :name, presence: true
end
