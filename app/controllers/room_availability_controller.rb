class RoomAvailabilityController < ApplicationController
  include Response

  # GET /room-availability
  def index
    booking_date = params[:booking_date]

    unless booking_date.present?
      return api({ message: 'booking_date is required.' }, 422)
    end

    rooms = MeetingRoom.where.not(
      id: Booking.where(booking_date: params[:booking_date]).pluck(:meeting_room_id)
    )

    api each_serializer(rooms, MeetingRoomSerializer)
  end
end
