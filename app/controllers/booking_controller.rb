class BookingController < ApplicationController
  include Response

  # POST /bookings
  def create
    is_booked = is_booked?(booking_params[:meeting_room_id], booking_params[:booking_date])

    unless !is_booked
      return api({ message: 'Meeting room has been booked.' }, 422)
    end

    booking = Booking.new(booking_params)

    if booking.save
      api serializer(booking, BookingSerializer), 201
    else
      api({ message: booking.errors }, 422)
    end
  end

  def is_booked?(room_id, date)
    return Booking.find_by(meeting_room_id: booking_params[:meeting_room_id], booking_date: booking_params[:booking_date])
  end

  private

    def booking_params
      params.permit(
        :user_id,
        :meeting_room_id,
        :booking_date
      )
    end
end
