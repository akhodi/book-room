Rails.application.routes.draw do
  post 'booking', to: 'booking#create'
  resources :room_availability, only: [:index], path: 'room-availability'
end
